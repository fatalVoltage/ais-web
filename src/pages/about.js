import React from "react";
//  import axios from "axios";
import { Heading, Frame, Paragraph, Section } from "../components";

export default class About extends React.Component {
  state = {
    content: {
      heading: "Hello",
      paragraph: "Hello"
    }
  };

  render() {
    const { content } = this.state;
    return (
      <Frame>
        <Section>
          <div style={{ marginTop: "5em" }}>
            <Heading>{content.heading}</Heading>
            <Paragraph>{content.paragraph}</Paragraph>
          </div>
        </Section>
      </Frame>
    );
  }
}

// async fetchComponentStrings() {
//   await axios({ method: "GET", url: "http://localhost:5000/get/about" })
//     .then(res => this.setState({ content: res.data }))
//     .catch(e => window.alert(e));
// }

// componentDidMount() {
//   this.fetchComponentStrings();
// }
