import React from "react";
import { Flex, Box } from "grid-styled";
import { Container, Card, Image, Button } from "semantic-ui-react";
import { Section, Heading, Link } from "../../components";

const content = {
  heading: "Latest Stories",
  subheading: "The latest news about the fun we have!",
  stories: [
    {
      by: "Simon",
      photo:
        "http://www.njsportshots.com/wp-content/uploads/HighSchoolSports5jpg.jpg",
      title: "Our new cheerleading team!",
      text:
        "Happy to introduce our new team, all students wishing to participate are welcome!",
      link: "/",
      when: "3 days ago"
    },
    {
      by: "Simon",
      photo:
        "http://www.njsportshots.com/wp-content/uploads/HighSchoolSports5jpg.jpg",
      title: "Our new cheerleading team!",
      text:
        "Happy to introduce our new team, all students wishing to participate are welcome!",
      link: "/",
      when: "3 days ago"
    },
    {
      by: "Simon",
      photo:
        "http://www.njsportshots.com/wp-content/uploads/HighSchoolSports5jpg.jpg",
      title: "Our new cheerleading team!",
      text:
        "Happy to introduce our new team, all students wishing to participate are welcome!",
      link: "/",
      when: "3 days ago"
    },
    {
      by: "Simon",
      photo:
        "http://www.njsportshots.com/wp-content/uploads/HighSchoolSports5jpg.jpg",
      title: "Our new cheerleading team!",
      text:
        "Happy to introduce our new team, all students wishing to participate are welcome!",
      link: "/",
      when: "3 days ago"
    }
  ]
};

export default () => (
  <Section theme="tertiary" style={{ padding: "2.5em 0", textAlign: "center" }}>
    <Heading size="xxl">{content.heading}</Heading>
    <Heading size="big" style={{ marginBottom: "1em" }}>
      {content.subheading}
    </Heading>
    <Flex
      flexDirection={["column", "row", "row"]}
      justifyContent="center"
      alignItems="center"
      flexWrap="wrap"
    >
      {content.stories.map((s, i) => (
        <Box key={i} mr={[0, "1em", "2em"]} mb={["1em", "1em", 0]}>
          <Card>
            <Image src={s.photo} />
            <Card.Content>
              <Card.Header>{s.title}</Card.Header>
              <Card.Meta>
                <span className="date">
                  {s.when} by {s.by}
                </span>
              </Card.Meta>
              <Card.Description>{s.text}</Card.Description>
            </Card.Content>
            <Card.Content extra>
              <Link to={s.link}>more</Link>
            </Card.Content>
          </Card>
        </Box>
      ))}
    </Flex>
  </Section>
);
