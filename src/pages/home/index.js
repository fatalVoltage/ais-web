import React from "react";
import Frame from "../../components/Frame";
import Hero from "./Hero";
import InfoPanel from "./InfoPanel";
import Stories from "./Stories";

export default class Home extends React.Component {
  render() {
    return (
      <Frame>
        <Hero />
        <Stories />
        <InfoPanel />
      </Frame>
    );
  }
}
