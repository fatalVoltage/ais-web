import React, { Fragment } from "react";
import { Flex, Box } from "grid-styled";
import {
  Section,
  Heading,
  Paragraph,
  Button,
  Link,
  media
} from "../../components";
import styled from "styled-components";

const content = [
  {
    height: [780, 550, 550, 400, 400],
    orientation: "left",
    img: {
      size: [150, 200, 200, 200, 200],
      address: "https://www.freeiconspng.com/uploads/atom-png-0.png"
    },
    heading: "Campus - includes all facilities",
    text:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse nec tristique lectus. Aliquam viverra arcu sit amet dolor porta, nec hendrerit nunc ultrices. Etiam dignissim massa sed velit molestie, non tempus justo porta. "
  },
  {
    height: [780, 550, 550, 400, 400],
    orientation: "right",
    img: {
      size: [200, 200, 250, 400, 400],
      address:
        "https://png2.kisspng.com/20180227/hgq/kisspng-scholastic-book-fairs-reading-clip-art-school-children-5a95e6a0acf271.9056465215197733447084.png"
    },
    heading: "Staff - proffessional and experienced",
    text:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam dignissim massa sed velit molestie, non tempus justo porta. ",
    button: { to: "/", value: "More" }
  },
  {
    height: [620, 450, 450, 400, 400],
    orientation: "left",
    img: {
      size: [200, 200, 250, 400, 400],
      address:
        "https://vemo.com/wp-content/uploads/2018/07/Item-247-1024x786.png"
    },
    heading: "Facilities",
    text:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur sit amet viverra nisi, a ultricies ipsum.",
    button: { to: "/", value: "More" }
  }
];

const ResponsiveFlex = styled(Flex)`
  ${media.small`
    height: ${props => props.height[0]}px;
  `} 
  ${media.phone`
    height: ${props => props.height[1]}px;
  `} 
  ${media.tablet`
    height: ${props => props.height[2]}px;
  `} 
  ${media.desktop`
    height: ${props => props.height[3]}px;
  `} 
  ${media.large`
    height: ${props => props.height[4]}px;
  `} 
`;

const ResponsiveImage = styled.img`
  ${media.small`
    max-width: ${props => props.size[0]}px;
  `} 
  ${media.phone`
    max-width: ${props => props.size[1]}px;
  `} 
  ${media.tablet`
    max-width: ${props => props.size[2]}px;
  `} 
  ${media.desktop`
    max-width: ${props => props.size[3]}px;
  `} 
  ${media.large`
    max-width: ${props => props.size[4]}px;
  `} 
`;

const StyledParagraph = styled(Paragraph)`
  ${media.tablet`
    max-width: 70%; 
  `};
`;

export default () => (
  <Fragment>
    {content.map((c, i) => (
      <Section theme={c.orientation === "left" ? "alternate" : "main"} key={i}>
        <ResponsiveFlex
          flexDirection={["column", "row", "row"]}
          height={c.height}
          justifyContent="center"
          alignItems="center"
        >
          <Box
            order={c.orientation === "left" ? 1 : 2}
            width={[1, 1 / 2, 1 / 2]}
            style={{ alignSelf: "center" }}
          >
            <Heading size="xxl">{c.heading}</Heading>
            <StyledParagraph>{c.text}</StyledParagraph>
            {c.button && (
              <Link to={c.button.to}>
                <Button basic>{c.button.value}</Button>
              </Link>
            )}
          </Box>
          <Box
            order={c.orientation === "left" ? 2 : 1}
            width={[1, 1 / 2, 1 / 2]}
            style={{ alignSelf: "center", textAlign: "center" }}
          >
            <ResponsiveImage src={c.img.address} size={c.img.size} />
          </Box>
        </ResponsiveFlex>
      </Section>
    ))}
  </Fragment>
);
