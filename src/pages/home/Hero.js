import React from "react";
// import styled from "styled-components";
import { Flex, Box } from "grid-styled";
import { Section, Heading, Paragraph, Button } from "../../components";

export default () => (
  <Section
    background="linear-gradient(0deg, black 0%, rgba(29, 28, 26, 0) 70%), url(&quot;/hero_background.jpeg&quot;) no-repeat center"
    style={{ height: "80vh" }}
  >
    <Flex
      justifyContent="center"
      alignItems="center"
      style={{ height: "100%" }}
    >
      <Box width={[1, 1, 1 / 2]} style={{ textAlign: "center" }}>
        <Heading size="xxl" bold>
          "Exceptional quality education, with excellent staff and extra
          activities for all students"
        </Heading>
        <Heading size="big" style={{ marginBottom: "2em" }}>
          School Compared Review 2018
        </Heading>
        <Button basic>Enquire</Button>
      </Box>
    </Flex>
  </Section>
);
