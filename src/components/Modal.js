import React, { Fragment } from "react";
import styled from "styled-components";

const Screen = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  z-index: 1;
  height: 100vh;
  background: ${props => props.theme.background};
`;

export default class Modal extends React.Component {
  state = {
    visible: false
  };

  toggleModalVisibility = () => {
    console.log("clicked");
    this.setState({ visible: !this.state.visible });
  };

  render() {
    const { visible } = this.state;
    const { trigger, content } = this.props;

    return (
      <Fragment>
        {visible && (
          <Screen>
            {React.cloneElement(content, {
              toggleModalVisibility: this.toggleModalVisibility
            })}
          </Screen>
        )}
        {React.cloneElement(trigger, {
          toggleModalVisibility: this.toggleModalVisibility
        })}
      </Fragment>
    );
  }
}
