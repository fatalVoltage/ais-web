const WHITE = "#FFFFFF";
const NAVY_BLUE = "#153679";
const ZAMBEZI = "#585858";
const WHITE_SMOKE = "#F1F1F1";
const BLACK = "#000000";

const main = {
  primary: WHITE,
  secondary: ZAMBEZI,
  text: WHITE,
  background: NAVY_BLUE
};

const alternate = {
  primary: NAVY_BLUE,
  secondary: WHITE,
  text: BLACK,
  background: WHITE
};

const tertiary = {
  primary: BLACK,
  secondary: NAVY_BLUE,
  text: "gray",
  background: WHITE_SMOKE
};

const themes = {
  main,
  alternate,
  tertiary
};

export { themes };
