import React from "react";
import styled, { css } from "styled-components";

const StyledParagraph = styled.p`
  color: ${props =>
    props.color ? props.theme[props.color] : props.theme.text};
  font-size: ${props => (props.size ? props.size : "1.5em")};
  ${props =>
    props.bold &&
    css`
      font-weight: bold;
    `};
`;

const Paragraph = props => (
  <StyledParagraph {...props}>{props.children}</StyledParagraph>
);

export default Paragraph;
