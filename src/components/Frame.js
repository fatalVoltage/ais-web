import React from "react";
import styled from "styled-components";
import Menu from "./Menu";

const FrameContainer = styled.div`
  width: 100%;
  min-height: 100vh;
`;

const Frame = props => (
  <FrameContainer>
    <Menu />
    {props.children}
  </FrameContainer>
);

export default Frame;
