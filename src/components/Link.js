import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import styled from "styled-components";
import media from "./media";

const StyledLink = styled(Link)`
  color: ${props => props.theme[props.color]};

  &:focus,
  &:hover,
  &:link,
  &:active {
    text-decoration: none;
  }
  display: block;
  cursor: pointer;

  &:hover {
    color: ${props => props.theme.primary};
    font-weight: bold;
  }

  ${media.desktop` line-height: 1.5em;`};
`;

class UiLink extends React.Component {
  render() {
    return <StyledLink {...this.props}>{this.props.children}</StyledLink>;
  }
}

UiLink.propTypes = {
  color: PropTypes.string,
  to: PropTypes.string
};

UiLink.defaultProps = {
  color: "text"
};

export default UiLink;
