import React from "react";
import styled, { css } from "styled-components";

const getSize = size => {
  if (size === "xxl") {
    return "3em";
  } else if (size === "big") {
    return "2em";
  } else if (size === "small") {
    return "0.5em";
  }
};

const Heading = styled.h1`
  line-height: 1em;
  font-size: ${props => (props.size ? getSize(props.size) : "1em")};
  color: ${props =>
    props.color ? props.theme[props.color] : props.theme.primary};
  ${props =>
    props.wide &&
    css`
      width: 100%;
    `};
  ${props =>
    props.bold &&
    css`
      font-weight: bold;
    `};
`;

export default props => <Heading {...props}>{props.children}</Heading>;
