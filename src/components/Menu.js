import React, { Fragment } from "react";
import chunk from "lodash/chunk";
import styled from "styled-components";
import media from "./media";
import { ShowMobile, ShowDesktop } from "./Responsive";
import { Flex, Box } from "grid-styled";
import Section from "./Section";
import Button from "./Button";
import Link from "./Link";
import Modal from "./Modal";
import Collapsable from "./Collapsable";
import Heading from "./Heading";

const MenuContainer = styled.div`
  position: fixed;
  width: 100vw;
  min-height: 100vh;
  background: ${props => props.theme.background};
  z-index: 1;
`;

const DesktopOnlyBox = styled(Box)`
  display: none;
  ${media.large`
    display: flex; 
  `};
`;

const SiteHeader = ({ toggleModalVisibility }) => (
  <Section
    theme="alternate"
    style={{
      paddingTop: "0.5em",
      paddingBottom: "0.5em",
      zIndex: 2
    }}
  >
    <Flex
      justifyContent="center"
      alignItems="center"
      style={{
        zIndex: 2
      }}
    >
      <Box width={[1 / 2, 1 / 3, 1 / 3]}>
        <img src="/bis_logo.png" style={{ width: 100 }} />
      </Box>
      <DesktopOnlyBox width={[1 / 4, 1 / 3, 1 / 3]} mr={[0, 0, "2em"]}>
        <Button style={{ marginRight: "2em" }} basic thin>
          Enroll
        </Button>
        <Button basic thin>
          Get a call back
        </Button>
      </DesktopOnlyBox>
      <Box width={[1 / 4, 1 / 3, 1 / 3]} style={{ textAlign: "right" }}>
        <Button onClick={toggleModalVisibility} basic>
          Menu
        </Button>
      </Box>
    </Flex>
  </Section>
);

const MobileMenu = ({ sections, toggleModalVisibility }) => (
  <MenuContainer>
    <SiteHeader toggleModalVisibility={toggleModalVisibility} />
    <Flex
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
      style={{ marginTop: "3em" }}
    >
      {sections.map((s, i) => (
        <Box key={i} mb="1em" style={{ borderBottom: "1px solid black" }}>
          <Collapsable
            parent={
              <Heading bold size="big">
                {s.heading}
              </Heading>
            }
            child={
              <Fragment>
                {s.links.map((l, u) => (
                  <Link
                    to={l.to}
                    key={u}
                    style={
                      u === s.links.length - 1 ? { paddingBottom: "1em" } : {}
                    }
                  >
                    {l.key}
                  </Link>
                ))}
              </Fragment>
            }
          />
        </Box>
      ))}
    </Flex>
  </MenuContainer>
);

const DesktopMenu = ({ sections, toggleModalVisibility }) => (
  <MenuContainer>
    <SiteHeader toggleModalVisibility={toggleModalVisibility} />
    <Flex
      flexDirection="row"
      alignItems="start"
      justifyContent="center"
      style={{ padding: "1em", marginTop: "3em", fontSize: "1.3em" }}
    >
      {chunk(sections, 2).map((s, i) => (
        <Box key={i} mr="4em">
          {s.map((sb, u) => (
            <Flex flexDirection="column" key={u}>
              <Heading bold size="big">
                {sb.heading}
              </Heading>
              <Box mb="0.5em">
                {sb.links.map((l, k) => (
                  <Link key={k} to={l.to}>
                    {l.key}
                  </Link>
                ))}
              </Box>
            </Flex>
          ))}
        </Box>
      ))}
    </Flex>
  </MenuContainer>
);

const content = [
  {
    heading: "About us",
    links: [
      { key: "Welcome from the CEO", to: "/welcome" },
      { key: "Vision, Mission & Values", to: "" },
      { key: "Our team", to: "" },
      { key: "School policies", to: "" },
      { key: "Our location", to: "" },
      { key: "Campus & Facilities", to: "" },
      { key: "Our accreditation & Membership", to: "" },
      { key: "STEAM School", to: "" },
      { key: "Contact us", to: "" }
    ]
  },
  {
    heading: "Admissions",
    links: [
      { key: "Overview", to: "" },
      { key: "Policy", to: "" },
      { key: "Process", to: "" },
      { key: "Tuition & Fees", to: "" }
    ]
  },
  {
    heading: "Learning",
    links: [
      { key: "Our philosophy of Education", to: "" },
      { key: "British Education System", to: "" },
      { key: "Our Curriculum", to: "" },
      { key: "Student Support", to: "" },
      { key: "STEAM Education", to: "" },
      { key: "Work Experience Programme", to: "" },
      { key: "Higher Education Guidance", to: "" },
      { key: "Career education & guidance", to: "" }
    ]
  },
  {
    heading: "Extra-curricular",
    links: [
      { key: "Internet-based Activities", to: "" },
      { key: "Sports Activities", to: "" },
      { key: "Creative & Performing Arts", to: "" },
      { key: "Student Council", to: "" },
      { key: "Languages & Culture", to: "" },
      { key: "Outdoor Education", to: "" },
      { key: "Specialist Clubs", to: "" },
      { key: "Creativity, Action, Service (CAS)", to: "" }
    ]
  },
  {
    heading: "For Parents",
    links: [
      { key: "Parental Engagement", to: "" },
      { key: "The Parent Association (TPA)", to: "" },
      { key: "Parent Workshops", to: "" },
      { key: "School Uniforms", to: "" },
      { key: "Health Services", to: "" },
      { key: "Food Services", to: "" },
      { key: "School Transport", to: "" },
      { key: "Student Support", to: "" },
      { key: "Feedback & Suggestions", to: "" }
    ]
  },
  {
    heading: "Careers",
    links: [
      { key: "Why STEAM Education?", to: "" },
      { key: "How we hire", to: "" },
      { key: "Salary & Benefits", to: "" },
      { key: "Professional Development", to: "" },
      { key: "Child Protection", to: "" },
      { key: "Living in Sweden", to: "" },
      { key: "How to apply?", to: "" },
      { key: "Current vacancies", to: "" }
    ]
  },
  {
    heading: "Why STEAM?",
    links: [
      { key: "New ways to learn", to: "" },
      { key: "Academic performance", to: "" },
      { key: "World-class", to: "" },
      { key: "International Curriculum", to: "" },
      { key: "Extra-curricular activities", to: "" },
      { key: "Exceptional Facilities", to: "" },
      { key: "Global classrooms", to: "" },
      { key: "The Latest Technology", to: "" },
      { key: "Student Support", to: "" },
      { key: "Great teachers", to: "" },
      { key: "Professional principals", to: "" },
      { key: "Family Engagement", to: "" },
      { key: "Supportive School Community", to: "" }
    ]
  }
];

export default () => (
  <Section theme="alternate" style={{ padding: "0" }}>
    <ShowMobile>
      <Modal
        trigger={<SiteHeader />}
        content={<MobileMenu sections={content} />}
      />
    </ShowMobile>
    <ShowDesktop>
      <Modal
        trigger={<SiteHeader />}
        content={<DesktopMenu sections={content} />}
      />
    </ShowDesktop>
  </Section>
);
