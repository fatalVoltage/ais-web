import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { ThemeProvider } from "styled-components";
import { themes } from "./themes";
import media from "./media";

const SectionContainer = styled.div`
  background: ${props =>
    props.background ? props.background : props.theme.background};
  margin: 0 auto;
  ${media.small`
    padding: 0.5em 1em;
  `}
  ${media.phone`
    padding: 1em 1.5em; 
  `}
  ${media.tablet`
    padding: 1.5em 2em; 
  `}
  ${media.desktop`
    padding: 1.5em 2.5em;
  `}
  ${media.large`
    padding: 2em 2.5em;
  `}
  ${media.xxl`
    padding: 2em 10em;
  `}
`;

export default class Section extends React.Component {
  render() {
    return (
      <ThemeProvider theme={themes[this.props.theme]}>
        <SectionContainer
          style={this.props.style}
          background={this.props.background}
        >
          {this.props.children}
        </SectionContainer>
      </ThemeProvider>
    );
  }
}

Section.propTypes = {
  theme: PropTypes.string,
  style: PropTypes.object,
  background: PropTypes.string
};

Section.defaultProps = {
  theme: "main"
};
