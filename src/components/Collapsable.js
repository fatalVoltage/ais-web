import React, { Fragment } from "react";

export default class Collapsable extends React.Component {
  state = {
    toggled: false
  };

  toggleContent = () => {
    this.setState(prev => {
      return { toggled: !prev.toggled };
    });
  };

  render() {
    const { parent, child } = this.props;
    const { toggled } = this.state;
    return (
      <Fragment>
        <span onClick={this.toggleContent}>{parent}</span>
        {toggled && child}
      </Fragment>
    );
  }
}
