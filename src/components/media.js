import { css } from "styled-components";

const sizes = {
  xxl: 1500,
  large: 1024,
  desktop: 992,
  tablet: 768,
  phone: 376,
  small: 100
};

// Iterate through the sizes and create a media template
const media = Object.keys(sizes).reduce((acc, label) => {
  acc[label] = (...args) => css`
    @media (min-width: ${sizes[label]}px) {
      ${css(...args)};
    }
  `;

  return acc;
}, {});

export default media;
