import styled from "styled-components";
import media from "./media";

export const ShowMobile = styled.div`
  @media (min-width: 769px) {
    display: none;
  }
`;

export const ShowDesktop = styled.div`
  @media (max-width: 769px) {
    display: none;
  }
`;
