import React from "react";
import PropTypes from "prop-types";
import styled, { css } from "styled-components";

const StyledButton = styled.a`
  padding: ${props => (props.thin ? "0.5em 0.25em" : "0.5em 1em")};
  text-decoration: none;
  background: ${props => props.theme.primary};
  color: ${props =>
    props.color ? props.theme[props.color] : props.theme.secondary};
  cursor: pointer;
  width: 50%;

  ${props =>
    props.wide &&
    css`
      width: 100%;
    `};

  font-size: ${props => (props.size ? props.size : "1.2em")};
  text-align: center;

  ${props =>
    props.basic &&
    css`
      background: transparent;
      border: 1px solid ${props => props.theme.primary};
      color: ${props => props.theme.primary};
    `};
`;

const Button = props => (
  <StyledButton {...props}>{props.children}</StyledButton>
);

Button.propTypes = {
  wide: PropTypes.bool,
  basic: PropTypes.bool,
  color: PropTypes.string,
  size: PropTypes.string,
  thin: PropTypes.bool
};

export default Button;
