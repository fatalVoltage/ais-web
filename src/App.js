import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import { Home, About, Welcome } from "./pages";

export default class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/about" component={About} />
          <Route path="/welcome" component={Welcome} />
        </Switch>
      </BrowserRouter>
    );
  }
}
